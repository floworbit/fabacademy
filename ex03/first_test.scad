edge_size=2;
width=160;
height=90;
thickness=6;
led_divisor=10;
union(){
difference(){
    cube([width,height,thickness]);
    translate([edge_size/2, edge_size/2, -1]){
    cube([width-edge_size,height-edge_size,thickness+2]);
    }
}
edge_size2=thickness;
thickness2=edge_size;
difference(){
    cube([width,height,thickness2]);
    translate([edge_size2/2, edge_size2/2, -1]){
    cube([width-edge_size2,height-edge_size2,thickness2+2]);
    }
}

difference(){
translate([0,0,thickness]){
difference(){
    cube([width,height,thickness2]);
    translate([edge_size2/2, edge_size2/2, -1]){
    cube([width-edge_size2,height-edge_size2,thickness2+2]);
    }
    }
}

for (i = [0:(width/led_divisor)-1]){
    translate([(i*10)+led_divisor/2, edge_size2/4, thickness-thickness2]){
        cylinder(r=1, h=edge_size*2+2);
     }
     
     translate([(i*10)+led_divisor/2, height-edge_size2/4, thickness-thickness2]){
        cylinder(r=1, h=edge_size*2+2);
     }
}
for (i = [1:(height/led_divisor)-2]){
    translate([edge_size2/4, (i*led_divisor)+led_divisor/2, thickness-thickness2]){
        cylinder(r=1, h=edge_size*2+2);
     }
     
     translate([width-edge_size2/4, (i*led_divisor)+led_divisor/2, thickness-thickness2]){
        cylinder(r=1, h=edge_size*2+2);
     }
 }

}
cube([width/3,height/3,edge_size]);
translate([width/4, height-height/5, 0]){
cube([width/2,height/5,edge_size]);
}
}