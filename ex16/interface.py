#! /usr/bin/env python3
from tkinter import *
from time import sleep
import serial

manual = True # Is used to decide if autoWorker() terminates or loops
autoStatus = True # In auto mode start with sending the "on" signal
ser = serial.Serial(
    port='/dev/ttyACM0', # Adjust accordingly, easiest way might be to check the Arduino IDE
    baudrate=9600,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.SEVENBITS
)

def autoWorker():
    global autoStatus # Global variables have to be marked with the "global" keyword
    if not manual:
        if autoStatus:
            sendSerial('1')
        else:
            sendSerial('0')
        autoStatus = not autoStatus
        # In order not to block the thread running the GUI, TkInters after() method is used
        # The multiprocessing library can not be used here
        # The lambda expression just recursively calls this function to create a loop
        root.after(10000//sspeed.get(), lambda: autoWorker())

def auto():
    global manual
    manual = True # I don't remember why I did this and I'm not sure if it even makes any sense ¯\_(ツ)_/¯
    manual = False
    autoWorker()

def sendSerial(signal):
    print(signal) # This is for debugging
    ser.write(str.encode(signal))

def on():
    global manual
    manual = True
    sendSerial('1')

def off():
    global manual
    manual = True
    sendSerial('0')

# Here the GUI is created and all the previously defined functions are bound to the control elements 
# pack() is used to define the relative placement of the elements
root = Tk() # Create the base window
ltitle = Label(text="FabAcademy - Exercise 16", font=("Helvetica", 15))
ltitle.pack()
bon = Button(text="On", command=on)
boff = Button(text="Off", command=off)
bauto = Button(text="Auto", command= lambda: auto())
bon.pack(side=LEFT)
boff.pack(side=LEFT)
bauto.pack(side=LEFT)
lscaledesc = Label(text="switches/10s", font=("Helvetica", 10))
lscaledesc.pack()
sspeed = Scale(root, from_=1, to=20, orient=HORIZONTAL)
sspeed.pack()
bexit = Button(text="Exit", command=sys.exit)
bexit.pack(side=RIGHT)
root.mainloop() # Everything is prepared and we can pass the control to TkInter

