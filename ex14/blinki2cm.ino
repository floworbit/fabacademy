#include <Wire.h>

void setup() {
  Wire.begin();
}

// the loop function runs over and over again forever
void loop() {
  delay(2000);                       // wait for a second
  Wire.beginTransmission(10); // transmit to device #10
  Wire.write('a'); // sends x
  Wire.endTransmission();
  delay(2000);                       // wait for a second
  Wire.beginTransmission(9); // transmit to device #9
  Wire.write('a'); // sends x
  Wire.endTransmission();
}
