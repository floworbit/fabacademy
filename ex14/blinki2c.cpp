#include "TinyWire.h"
#include <avr/io.h>
#include <util/delay.h>

void onReceive(int foo){
	PORTA |= (1 << 7);	
	_delay_ms(200);
	PORTA &= ~(1 << 7);

}

int main() {
	DDRA |= 0b10000000;
	TinyWire.begin(10);
	TinyWire.onReceive(onReceive);
}

