#include <twi.h>
#include <TinyWire.h>

void setup() {
  TinyWire.begin(9);   
  //TinyWire.begin(10);
  TinyWire.onReceive(receiveEvent); // register event
  //pinMode(A7, OUTPUT);
  pinMode(PB4, OUTPUT);
}

void loop() {
}

void receiveEvent(int howMany) {
  while (TinyWire.available() > 0) { // loop through all but the last
     char c = TinyWire.read();
    if(c == 'a'){
      //digitalWrite(A7, HIGH);
      digitalWrite(PB4, HIGH);
      delay(500);
      digitalWrite(PB4, LOW);
      //digitalWrite(A7, LOW);
    }
  }
  
}
